﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace _16._08_homework
{
    class Program
    {
        static void Main(string[] args)
        {
            Car car1 = new Car("Suzuki", "Swift", 1995, "Blue", 1212, 5);
            Car car2 = new Car("Suzuki", "Euroswift", 2000, "White", 3131, 5);
            Car car3 = new Car("Skoda", "Fabia", 2006, "Bordeaux", 5252, 5);
            Car car4 = new Car("Skoda", "Yeti", 2012, "Platinum", 4334, 5);
            Car[] cars = {car1,car2, car3,car4};

            XmlSerializer CarXmlSerializer = new XmlSerializer(typeof(Car[]));
            StringWriter stringWriter = new StringWriter();
            CarXmlSerializer.Serialize(stringWriter, cars);
            using (Stream stream = new FileStream(@"..\..\xmlfile.xml", FileMode.Create))
            {
                CarXmlSerializer.Serialize(stream, cars);
            }

            //desirializing. Not finished

            Car carFromFile = null;
            using (Stream stream1 = new FileStream(@"..\..\readxmlfile.xml", FileMode.Open))
                {
                   carFromFile = CarXmlSerializer.Deserialize(stream1) as Car;
                }


            Car[] carsReadFromFile = null;
            using (Stream stream2 = new FileStream(@"..\..\readxmlfile.xml", FileMode.Open))
            {
                carsReadFromFile = (Car[])CarXmlSerializer.Deserialize(stream2);

            }

        }
    }
}
