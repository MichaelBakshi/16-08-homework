﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace _16._08_homework
{
    [XmlRoot("My list of cars")]
    public class Car
    {
        public string _model {get;set;}
        public string _brand { get; set;}
        [XmlElement("Shana")]
        public int _year { get; set; }
        [XmlAttribute("Color")]
        public string _color { get; set; }
        private int _codan;
        protected int _numberOfSeats;
        private string _fileName;

        public Car()
        {

        }

        public Car (string fileName)
        {
            this._fileName = fileName;
        }
        public Car(string model, string brand, int year, string color, int codan, int numberofseats)
        {
            this._brand = brand; 
            this._model = model;
            this._year = year;
            this._color = color;
            this._codan = codan;
            this._numberOfSeats = numberofseats;
        }

        public int GetCodan()
        {
            return _codan;
        }

        public int GetNumberOfSeats()
        {
            return _numberOfSeats;
        }

        protected void ChangeNumberOfSeats(int newNumberOfSeats)
        {
            this._numberOfSeats = newNumberOfSeats;
            Console.WriteLine("Number of seats now: "+_numberOfSeats);
        }

        public static void SerializeACar(string filename, Car car)
        {

        }

        public static void SerializeCarArray(string fileName, Car[] cars)
        {

        }

        //public static Car DeserializeACar (string filename)
        //{

        //}

        //public static  Car[]  DeserializeCarArray(string filename)
        //{
            
        //}

        public bool CarCompare (string filename)
        {
            return false; 
        }

        public override string ToString()
        {
            return $"Brand: {_brand} Model: {_model}  Year: {_year} Color: {_color} Number of Seats: {_numberOfSeats}";
        }


    }
}
